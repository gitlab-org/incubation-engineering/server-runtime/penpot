package main

import (
	_ "embed"
	"fmt"
	"net/http"
	"os"
)

//go:embed wellKnownResponse.json
var wellKnownResponse string

//go:embed token.json
var token string

//go:embed userInfo.json
var userInfo string

func main() {

	domain := os.Getenv("WORKSPACE_DOMAIN")

	http.HandleFunc("/.well-known/openid-configuration", func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("Well known endpoint called\n")
		response := fmt.Sprintf(wellKnownResponse, domain)
		w.Header().Add("Content-Type", "application/json")
		w.Write([]byte(response))
	})

	http.HandleFunc("/token", func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("Token endpoint called\n")
		w.Header().Add("Content-Type", "application/json")
		w.Write([]byte(token))
	})

	http.HandleFunc("/userinfo", func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("User Info endpoint called %s\n", userInfo)
		w.Header().Add("Content-Type", "application/json")
		w.Write([]byte(userInfo))
	})

	fmt.Printf("OIDC server starting...\n")

	err := http.ListenAndServe(":9999", nil)
	if err != nil {
		fmt.Printf("Error %s\n", err)
	}
}
