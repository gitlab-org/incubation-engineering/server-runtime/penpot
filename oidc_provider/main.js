const express = require('express')

const app = express()

const workspaceDomain = process.env.WORKSPACE_DOMAIN

app.get('/.well-known/openid-configuration', (req, res) => {

    console.log('Well known URL called')

    res.json({
        "issuer": "http://whatever:9999/",
        "authorization_endpoint": `https://9999-${workspaceDomain}/authorize`,
        "token_endpoint": "http://localhost:9999/token",
        "userinfo_endpoint": "http://localhost:9999/userinfo",
        "jwks_uri": "http://localhost:9999/.well-known/jwks.json",
        "response_types_supported": [
            "code",
            "id_token",
            "token id_token"
        ],
        "token_endpoint_auth_methods_supported": [
            "client_secret_basic"
        ],
    })
})

app.get('/authorize', (req, res) => {
    const redirect_uri = `https://${workspaceDomain}/api/auth/oauth/oidc/callback`
    const state = req.query["state"]
    console.log('state', state)
    const url = `${redirect_uri}?code=232132123123123123121231&state=${state}`
    console.log('Authorize called redirecting to ', url)
    res.redirect(url)
})

app.post('/token', (req, res) => {
    console.log('Token called')
    res.json({
        'access_token': '123',
        'refresh_token': '456'
    })
})

app.get('/userinfo', (req, res) => {
    res.json({
        "sub"                                   : "admin",
        "name"                                  : "Admin",
        "email"                                 : "admin@example.com",
    })
})

app.listen(9999, () => {
    console.log('Open ID Server started....')
})