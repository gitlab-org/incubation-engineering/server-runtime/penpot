package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"

	"github.com/devfile/library/pkg/devfile/generator"
	"github.com/devfile/library/pkg/devfile/parser"
	"github.com/devfile/library/pkg/devfile/parser/data/v2/common"
	namegen "github.com/dustinkirkland/golang-petname"
	apiv1 "k8s.io/api/core/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/cli-runtime/pkg/printers"
)

func main() {

	filename := os.Args[1]
	namegen.NonDeterministicMode()
	name := namegen.Generate(2, "-")

	data, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}

	x := true

	devfile, err := parser.ParseDevfile(parser.ParserArgs{
		Data:                          data,
		ConvertKubernetesContentInUri: &x,
	})
	if err != nil {
		log.Fatal(err)
	}

	containers, err := generator.GetContainers(devfile, common.DevfileOptions{})
	if err != nil {
		log.Fatal(err)
	}

	for i, container := range containers {
		components, err := devfile.Data.GetComponents(common.DevfileOptions{
			FilterByName: container.Name,
		})
		if err != nil {
			continue
		}

		if len(components) == 0 {
			continue
		}

		component := components[0]

		if component.Container.MountSources != nil && *component.Container.MountSources {

			path := "/home/workspaces"
			if component.Container.SourceMapping != "" {
				path = component.Container.SourceMapping
			}

			mount := apiv1.VolumeMount{
				MountPath: path,
				Name:      "main",
			}

			if component.Attributes.Exists("volumeSubPath") {
				mount.SubPath = component.Attributes.Get("volumeSubPath", nil).(string)
			}

			containers[i].VolumeMounts = append(container.VolumeMounts, mount)
		}

	}

	ic, err := generator.GetInitContainers(devfile)
	if err != nil {
		log.Fatal(err)
	}

	deployParams := generator.DeploymentParams{
		TypeMeta: generator.GetTypeMeta("Deployment", "apps/v1"),
		ObjectMeta: generator.GetObjectMeta(name, name, map[string]string{
			"name": name,
		}, make(map[string]string)),
		Containers: containers,
		Volumes: []apiv1.Volume{
			{
				Name: "main",
				VolumeSource: apiv1.VolumeSource{
					PersistentVolumeClaim: &apiv1.PersistentVolumeClaimVolumeSource{
						ClaimName: name,
					},
				},
			},
		},
		PodSelectorLabels: map[string]string{
			"name": name,
		},
		InitContainers: ic,
	}

	deployment, err := generator.GetDeployment(devfile, deployParams)
	if err != nil {
		log.Fatal(err)
	}

	pvc := &apiv1.PersistentVolumeClaim{
		TypeMeta: generator.GetTypeMeta("PersistentVolumeClaim", "v1"),
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: name,
		},
		Spec: apiv1.PersistentVolumeClaimSpec{
			AccessModes: []apiv1.PersistentVolumeAccessMode{v1.ReadWriteOnce},
			Resources: apiv1.ResourceRequirements{
				Requests: apiv1.ResourceList{
					apiv1.ResourceStorage: resource.MustParse("2Gi"),
				},
			},
		},
	}

	printer := printers.YAMLPrinter{}

	ns := &apiv1.Namespace{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Namespace",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: name,
		},
	}

	services, err := generator.GetService(devfile, generator.ServiceParams{
		SelectorLabels: map[string]string{
			"name": name,
		},
		TypeMeta: generator.GetTypeMeta("Service", "v1"),
		ObjectMeta: generator.GetObjectMeta(name, name, map[string]string{
			"name": name,
		}, make(map[string]string)),
	}, common.DevfileOptions{})
	if err != nil {
		log.Fatal(err)
	}

	// var dest io.Writer = os.Stdout
	var dest io.Writer = bytes.NewBuffer([]byte{})

	err = printer.PrintObj(ns, dest)
	if err != nil {
		log.Fatal(err)
	}

	err = printer.PrintObj(pvc, dest)
	if err != nil {
		log.Fatal(err)
	}

	err = printer.PrintObj(deployment, dest)
	if err != nil {
		log.Fatal(err)
	}

	err = printer.PrintObj(services, dest)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(dest)
}
